# 15 - Configuration Management with Ansible - Ansible Integration in Terraform

**Demo Project:**
Ansible Integration in Terraform

**Technologies used:**
Ansible, Terraform, AWS, Docker, Linux

**Project Description:**
- Create Ansible Playbook for Terraform integration 
- Adjust Terraform configuration to execute Ansible Playbook automatically, so once Terraform provisions a server, it executes an Ansible playbook that configures the server



15 - Configuration Management with Ansible
# This project was developed as part of the DevOps Bootcamp at Techword With NANA https://www.techworld-with-nana.com/devops-bootcamp
